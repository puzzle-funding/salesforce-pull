import os.path

from simple_salesforce import Salesforce
import configparser
import pandas as pd
import getpass
from pandasql import sqldf


def mysql(query):
    return sqldf(query, globals())


def connect_to_config(account, detail):
    config = configparser.ConfigParser()
    config.read(r'C:\Users\{}\Documents\config.ini'.format(getpass.getuser()))
    account_config = config[account]
    return account_config[detail]


def get_lead_info(user, password, token):
    sf = Salesforce(username=user, password=password, security_token=token)
    q1 = sf.query_all_iter("SELECT "
                           "Id, "
                           "Request_ID__c, "
                           "LastModifiedDate, "
                           "Email, "
                           "Request_Created_Date__c, "
                           "LeadSource, "
                           "Status, "
                           "Company, "
                           "Business_Turnover__c, "
                           "Company_Type__c, "
                           "Date_of_Birth__c, "
                           "CreatedById, "
                           "Request_Amount__c, "
                           "Loan_Purpose__c, "
                           "Industry, "
                           "BusinessRegistration__c, "
                           "Time_Trading__c "
                           "FROM "
                           "Lead")
    e = []
    for row in q1:
        d = {'id': row['Id'],
             'uuid': row['Request_ID__c'],
             'created_date': row['Request_Created_Date__c'],
             'last_modified': row['LastModifiedDate'],
             'email': row['Email'],
             'affiliate': row['LeadSource'],
             'status': row['Status'],
             'company': row['Company'],
             'turnover': row['Business_Turnover__c'],
             'company_type': row['Company_Type__c'],
             'date_of_birth': row['Date_of_Birth__c'],
             'created_by': row['CreatedById'],
             'request_amount': row['Request_Amount__c'],
             'loan_purpose': row['Loan_Purpose__c'],
             'sector': row['Industry'],
             'business_registration': row['BusinessRegistration__c'],
             'time_trading': row['Time_Trading__c']}
        e.append(d)
    return e


def get_opportunity_info(user, password, token):
    sf = Salesforce(username=user, password=password, security_token=token)
    q2 = sf.query_all_iter("SELECT "
                           "Id,"
                           "Request_ID__c, "
                           "CreatedDate, "
                           "LastModifiedDate, "
                           "Salesforce_Request_ID__c, "
                           "LeadSource, "
                           "Business_Turnoverr__c, "
                           "Company_Type__c, "
                           "Date_of_Birth__c, "
                           "CreatedById,"
                           "Request_Amount__c,"
                           "Loan_Purpose__c,"
                           "StageName,"
                           "Time_Trading__c  "
                           "FROM "
                           "Opportunity")
    e = []
    for row in q2:
        d = {'id': row['Id'],
             'request_id': row['Request_ID__c'],
             'uuid': row['Salesforce_Request_ID__c'],
             'created': row['CreatedDate'],
             'last_modified': row['LastModifiedDate'],
             'affiliate': row['LeadSource'],
             'status': row['StageName'],
             'turnover': row['Business_Turnoverr__c'],
             'company_type': row['Company_Type__c'],
             'date_of_birth': row['Date_of_Birth__c'],
             'created_by': row['CreatedById'],
             'request_amount': row['Request_Amount__c'],
             'loan_purpose': row['Loan_Purpose__c'],
             'time_trading': row['Time_Trading__c']}
        e.append(d)
    return e


def get_offer_info(user, password, token):
    sf = Salesforce(username=user, password=password, security_token=token)
    q3 = sf.query_all_iter("SELECT "
                           "Salesforce_Request_ID__c, "
                           "Application__c, "
                           "CreatedDate, "
                           "LastModifiedDate, "
                           "Lender_Name__c, "
                           "Lender_Product__c, "
                           "Name, "
                           "Is_Accepted_by_Lender__c, "
                           "Is_Rejected_by_Lender__c, "
                           "Is_Rejected_by_Applicant__c, "
                           "Offered_Amount__c, "
                           "Offer_is_Funded__c, "
                           "Funded_Amount__c, "
                           "Puzzle_Revenue__c "
                           "FROM "
                           "Application_Lender__c")
    e = []
    for row in q3:
        d = {'opportunity_id': row['Salesforce_Request_ID__c'],
             'application': row['Application__c'],
             'created': row['CreatedDate'],
             'last_modified': row['LastModifiedDate'],
             'lender_name': row['Lender_Name__c'],
             'product_id': row['Lender_Product__c'],
             'name': row['Name'],
             'accepted_by_lender': row['Is_Accepted_by_Lender__c'],
             'rejected_by_lender': row['Is_Rejected_by_Lender__c'],
             'rejected_by_applicant': row['Is_Rejected_by_Applicant__c'],
             'offered_amount': row['Offered_Amount__c'],
             'offer_funded': row['Offer_is_Funded__c'],
             'funded_amount': row['Funded_Amount__c'],
             'puzzle_revenue': row['Puzzle_Revenue__c']}
        e.append(d)
    return e


def get_product_detail(user, password, token):
    sf = Salesforce(username=user, password=password, security_token=token)
    q4 = sf.query_all_iter("SELECT "
                           "Id, "
                           "CreatedDate, "
                           "LastModifiedDate, "
                           "Commission_Percentage__c, "
                           "Is_Government_Backed__c, "
                           "Lender__c, "
                           "Name, "
                           "Notes__c "
                           "FROM "
                           "Lender_Product__c")
    e = []
    for row in q4:
        d = {'uuid': row['Id'],
             'created': row['CreatedDate'],
             'last_modified': row['LastModifiedDate'],
             'commission_percentage': row['Commission_Percentage__c'],
             'government_backed': row['Is_Government_Backed__c'],
             'lender': row['Lender__c'],
             'name': row['Name'],
             'notes': row['Notes__c']}
        e.append(d)
    return e


def get_lender_detail(user, password, token):
    sf = Salesforce(username=user, password=password, security_token=token)
    q5 = sf.query_all_iter("SELECT "
                           "Id"
                           "Lender_ID__c, "
                           "Name, "
                           "CreatedDate, "
                           "LastModifiedDate "
                           "FROM Lender__c")
    e = []
    for row in q5:
        d = {'id': row['Id'],
             'uuid': row['Lender_ID__c'],
             'created': row['CreatedDate'],
             'last_modified': row['LastModifiedDate'],
             'name': row['Name']}
        e.append(d)
    return e


def run_script(sql_string):
    user = connect_to_config('salesforce', 'user')
    password = connect_to_config('salesforce', 'password')
    token = connect_to_config('salesforce', 'token')
    if ' pzl_requests' in sql_string:
        lead_dic = get_lead_info(user, password, token)
        print('pzl_requests accessed')
    else:
        lead_dic = {}
    if ' pzl_applications' in sql_string:
        opportunity_dic = get_opportunity_info(user, password, token)
        print('pzl_applications accessed')
    else:
        opportunity_dic = {}
    if ' pzl_underwriting' in sql_string:
        offer_dic = get_offer_info(user, password, token)
        print('pzl_underwriting accessed')
    else:
        offer_dic = {}
    if ' pzl_products' in sql_string:
        product_dic = get_product_detail(user, password, token)
        print('pzl_products accessed')
    else:
        product_dic = {}
    if ' pzl_lenders' in sql_string:
        lender_dic = get_product_detail(user, password, token)
        print('pzl_lenders accessed')
    else:
        lender_dic = {}
    return lead_dic, opportunity_dic, offer_dic, product_dic, lender_dic


def read_input_sql():
    file_path = 'Input\input_sql'
    text_file = open(file_path, "r")
    sql = text_file.read()
    text_file.close()
    return sql


sql_query = read_input_sql()
leads, opportunities, offers, product_lenders, lender_details = run_script(sql_query)

pzl_requests = pd.DataFrame(leads)
pzl_applications = pd.DataFrame(opportunities)
pzl_underwriting = pd.DataFrame(offers)
pzl_products = pd.DataFrame(product_lenders)
pzl_lenders = pd.DataFrame(lender_details)

output_df = mysql(sql_query)
file_name = input("Enter filename:")
print("Filename is: " + file_name)
output_df.to_csv('Output/' + file_name + '.csv', index=False)
